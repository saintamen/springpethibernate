<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Pet Registration Form</title>

    <style>

        .error {
            color: #ff0000;
        }
    </style>

</head>

<body>

<h2>Pets Form</h2>

<form:form method="POST" modelAttribute="pet">
    <form:input type="hidden" path="id" id="id"/>
    <table>
        <tr>
            <td><label for="name">Name: </label> </td>
            <td><form:input path="name" id="name"/></td>
            <td><form:errors path="name" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="joiningDate">Owner: </label> </td>
            <td><form:input path="owner" id="joiningDate"/></td>
            <td><form:errors path="owner" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="age">Age: </label> </td>
            <td><form:input path="age" id="age"/></td>
            <td><form:errors path="age" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="height">Height: </label> </td>
            <td><form:input path="height" id="height"/></td>
            <td><form:errors path="height" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="weight">Weight: </label> </td>
            <td><form:input path="weight" id="weight"/></td>
            <td><form:errors path="weight" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="pureBred">pureBred: </label> </td>
            <td><form:input path="pureBred" id="pureBred"/></td>
            <td><form:errors path="pureBred" cssClass="error"/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>
</body>
</html>
<br/>
<br/>
Go back to <a href="<c:url value='/pets/list' />">List of All Employees</a>
</body>
</html>