<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Listing of Pets</title>
</head>
<body>
<c:choose>
    <c:when test="${containsMessage}">
        <h2>Message: ${message}</h2>
    </c:when>
</c:choose>
<table>
    <c:forEach items="${pets}" var="pet">
        <tr>
            <td>${pet.id}</td>
            <td>${pet.name}</td>
            <td>${pet.owner}</td>
            <td>${pet.weight}</td>
            <td>${pet.height}</td>
            <td>${pet.pureBred}</td>
            <td>${pet.age}</td>
            <td>${pet.petOwner}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
