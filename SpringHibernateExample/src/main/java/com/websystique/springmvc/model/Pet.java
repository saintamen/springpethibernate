package com.websystique.springmvc.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="pet")
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min=3, max=50)
    @Column(nullable = false)
    private String name;

    @Size(min=3, max=50)
    @Column(nullable = false)
    private String owner;

    @Column
    private int age;

    @Column
    private double height;

    @Column
    private double weight;

    @Column
    private boolean pureBred;

    @OneToOne
    private PetOwner petOwner;

    public Pet() {
        id = 0;
    }

    public Pet(String name, String owner, int age, double height, double weight, boolean pureBred) {
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.pureBred = pureBred;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isPureBred() {
        return pureBred;
    }

    public void setPureBred(boolean pureBred) {
        this.pureBred = pureBred;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner='" + owner + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", pureBred=" + pureBred +
                ", petOwner=" + petOwner +
                '}';
    }

    public PetOwner getPetOwner() {
        return petOwner;
    }

    public void setPetOwner(PetOwner petOwner) {
        this.petOwner = petOwner;
    }
}
