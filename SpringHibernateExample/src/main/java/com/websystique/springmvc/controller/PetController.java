package com.websystique.springmvc.controller;

import com.websystique.springmvc.dao.EmployeeDao;
import com.websystique.springmvc.dao.PetDao;
import com.websystique.springmvc.model.Employee;
import com.websystique.springmvc.model.Pet;
import com.websystique.springmvc.service.EmployeeService;
import com.websystique.springmvc.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
//który będzie się zajmował obsługą zapytań na adres (/pets/)
@RequestMapping("/pets/")
public class PetController {

    @Autowired
    PetService service;

    // i w domyślnej metodzie GET
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String showPet(ModelMap model) {
        return "PetsView";
    }

    @RequestMapping(value = {"/pettest"}, method = RequestMethod.GET)
    public String testPet(ModelMap model) {
        Pet zwierzak = new Pet("burek", "ja", 10, 10.0, 11.0, true);
        model.addAttribute("pet", zwierzak);


        return "PetsTestView";
    }

    @RequestMapping(value = {"/petdbtest"}, method = RequestMethod.GET)
    public String testPetDatabase(ModelMap model) {
        Pet zwierzak = new Pet("burek", "jam jest twoj bug", 10, 10.0, 11.0, true);

        try{
            service.savePet(zwierzak);
        }catch (Exception e){
            Logger.getLogger(PetController.class.getName())
                    .log(Level.WARNING, "error: " + e.getMessage());
            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @RequestMapping(value = {"/petslist"}, method = RequestMethod.GET)
    public String petsList(ModelMap model) {
        List<Pet> pets = service.findAllPets();
        model.addAttribute("pets", pets);

        return "PetsListing";
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String newPet(ModelMap model) {
        Pet pet = new Pet();
        model.addAttribute("pet", pet);

        return "PetsForm";
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.POST)
    public String newPetPost(@Valid Pet pet, BindingResult result, ModelMap model) {

        if(result.hasErrors()){
            // jezeli formularz jest bledny to przeladuj formularz
            return "PetsForm";
        }

        service.savePet(pet);

//        model.addAttribute("`" , "Successfully added record.");
//        model.addAttribute("containsMessage", true);
//        List<Pet> pets = service.findAllPets();
//        model.addAttribute("pets", pets);
        return "redirect:/success";
    }




}
